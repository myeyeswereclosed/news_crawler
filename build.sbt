name := "news_crawler"

version := "0.1"

scalaVersion := "2.13.8"

lazy val http4sVersion = "0.23.11"

lazy val flywayVersion = "8.5.8"

lazy val pureConfigVersion = "0.17.1"

lazy val scalaScraperVersion = "2.2.1"

lazy val h2databaseVersion = "2.1.210"
lazy val quillJdbcVersion = "3.16.3"

lazy val circeVersion = "0.14.1"

lazy val sangriaCirceVersion = "1.3.2"
lazy val sangriaVersion = "3.0.0"

lazy val log4catsVersion = "2.2.0"
lazy val logbackVersion = "1.2.3"

lazy val sttpVersion = "3.5.2"

lazy val scalaTestVersion = "3.2.11"
lazy val scalaMockVersion = "5.2.0"

lazy val betterFilesVersion = "3.9.1"

libraryDependencies ++= Seq(
  "org.http4s" %% "http4s-blaze-server" % http4sVersion,
  "org.http4s" %% "http4s-blaze-client" % http4sVersion,
  "org.http4s" %% "http4s-dsl" % http4sVersion,
  "org.http4s" %% "http4s-circe" % http4sVersion,

  "org.flywaydb" % "flyway-core" % flywayVersion,

  "com.github.pureconfig" %% "pureconfig" % pureConfigVersion,

  "net.ruippeixotog" % "scala-scraper_2.13" % scalaScraperVersion,

  "com.h2database" % "h2" % h2databaseVersion,
  "io.getquill" %% "quill-jdbc" % quillJdbcVersion,

  "io.circe" %% "circe-core" % circeVersion,
  "io.circe"             %% "circe-optics"        % circeVersion,
  "io.circe"             %% "circe-parser"        % circeVersion,
  "io.circe"             %% "circe-generic"        % circeVersion,

  "org.sangria-graphql" % "sangria_2.13" % sangriaVersion,
  "org.sangria-graphql" %% "sangria-circe" % sangriaCirceVersion,

  "org.typelevel" %% "log4cats-core"    % log4catsVersion,
  "org.typelevel" %% "log4cats-slf4j"   % log4catsVersion,

  "com.softwaremill.sttp.client3" %% "core" % sttpVersion,
  "com.softwaremill.sttp.client3" %% "http4s-backend" % sttpVersion,
  "com.softwaremill.sttp.client3" %% "async-http-client-backend-cats" % sttpVersion,

  "ch.qos.logback" % "logback-classic"      % logbackVersion,

  "org.scalatest" %% "scalatest" % scalaTestVersion % Test,
  "org.scalamock" %% "scalamock" % scalaMockVersion % Test,

  "com.github.pathikrit" % "better-files_2.13" % betterFilesVersion,

  "dev.zio" %% "zio-interop-cats" % "3.2.9.1"
)

scalacOptions ++= Seq(
  "-deprecation",
  "-language:higherKinds",
  "-language:postfixOps",
  "-feature",
  "-Xfatal-warnings"
)

coverageEnabled := true