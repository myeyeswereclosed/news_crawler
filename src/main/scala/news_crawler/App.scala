package news_crawler

import cats.Parallel
import cats.effect.{Async, LiftIO, Resource, Sync}
import cats.implicits._
import io.getquill.{H2JdbcContext, SnakeCase}
import news_crawler.application.config.Config.{AppConfig, ServerConfig}
import news_crawler.application.config.Config
import news_crawler.application.dependencies.Dependencies
import news_crawler.application.router.Router
import org.http4s.blaze.server.BlazeServerBuilder
import org.http4s.server.Server
import org.typelevel.log4cats.slf4j.Slf4jLogger
import org.typelevel.log4cats.{Logger, SelfAwareStructuredLogger}

object App {
  private val databaseKey = "database"

  implicit def logger[F[_]: Sync]: SelfAwareStructuredLogger[F] = Slf4jLogger.getLogger[F]

  def run[F[_]: Async: LiftIO: Parallel]: F[Unit] =
    for {
      config <- Config.load[F]
      _      <- Logger[F].debug("Config loaded")
      _ <- dbContext().use(context => program(config, context))
    } yield ()

  private def program[F[_]: Async: LiftIO](config: AppConfig, dbContext: H2JdbcContext[SnakeCase.type]): F[Unit] = {
    val dependencies = Dependencies[F](config, dbContext)

    for {
      _ <- dependencies.migrationTool.migrate()
      _ <- Logger[F].info("Migrations applied. Starting crawling system and server...")
      _ <- server(dependencies.router, config.server).use(_ => dependencies.scheduledCrawler.run())
    } yield ()
  }

  private def server[F[_]: Async](router: Router[F], config: ServerConfig): Resource[F, Server] =
    BlazeServerBuilder[F]
      .bindHttp(host = config.host, port = config.port)
      .withHttpApp(router.routes().orNotFound)
      .resource

  private def dbContext[F[_]: Sync](): Resource[F, H2JdbcContext[SnakeCase.type]] =
    Resource.make(
      Sync[F].delay(new H2JdbcContext(SnakeCase, databaseKey))
    )(context => logger.debug("Releasing db context") *> Sync[F].delay(context.close()))
}
