package news_crawler

import cats.effect.{IO, _}

object ZioMain extends IOApp.Simple {

  override def run: IO[Unit] = {
    val zioApp: zio.Task[Unit] =
      App.run[zio.Task](
        zio.interop.catz.asyncRuntimeInstance(zio.Runtime.default),
        zio.interop.catz.liftIOInstance(runtime),
        zio.interop.catz.parallelInstance
      )

    zio.Runtime.default.unsafeRun(zioApp.exitCode)

    IO.unit
  }
}
