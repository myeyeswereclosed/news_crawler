package news_crawler.domain

case class NewsLink(title: String, link: String)

object NewsLink {
  type News = Seq[NewsLink]
}
