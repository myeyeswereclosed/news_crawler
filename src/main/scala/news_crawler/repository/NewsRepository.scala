package news_crawler.repository

import news_crawler.domain.NewsLink

trait NewsRepository[F[_]] {

  def find(news: Seq[NewsLink]): F[Seq[NewsLink]]

  def add(news: Seq[NewsLink]): F[Seq[NewsLink]]

  def list(): F[Seq[NewsLink]]
}
