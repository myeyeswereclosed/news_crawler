package news_crawler.repository

import cats.effect.kernel.Sync
import cats.implicits._
import io.getquill.{H2JdbcContext, SnakeCase}
import news_crawler.domain.NewsLink

class QuillNewsRepository[F[_]: Sync](val ctx: H2JdbcContext[SnakeCase.type]) extends NewsRepository[F] {
  import ctx._

  implicit val table = schemaMeta[NewsLink]("headlines")

  def find(news: Seq[NewsLink]): F[Seq[NewsLink]] =
    Sync[F].blocking(
      run(
        quote {
          query[NewsLink].filter(n => liftQuery(news.map(_.link)).contains(n.link))
        }
      )
    )

  def add(news: Seq[NewsLink]): F[Seq[NewsLink]] =
    Sync[F]
      .blocking(
        run(
          quote {
            liftQuery(news).foreach(n => query[NewsLink].insertValue(n))
          }
        )
      )
      .map(_ => news)

  def list(): F[Seq[NewsLink]] = Sync[F].blocking(run(quote(query[NewsLink])))

}

object QuillNewsRepository {

  def apply[F[_]: Sync](context: H2JdbcContext[SnakeCase.type]) = new QuillNewsRepository[F](context)

}
