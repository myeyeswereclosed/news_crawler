package news_crawler.application.migration

trait MigrationTool[F[_]] {

  def migrate(): F[Unit]

}
