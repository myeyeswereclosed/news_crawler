package news_crawler.application.migration

import cats.effect.Sync
import news_crawler.application.config.Config.MigrationConfig
import org.flywaydb.core.Flyway

class FlywayMigrationTool[F[_]: Sync](config: MigrationConfig) extends MigrationTool[F] {

  def migrate(): F[Unit] =
    Sync[F].delay(
      Flyway
        .configure()
        .dataSource(config.url, config.user, config.password)
        .load()
        .migrate()
    )

}

object FlywayMigrationTool {

  def apply[F[_]: Sync](config: MigrationConfig) = new FlywayMigrationTool(config)

}
