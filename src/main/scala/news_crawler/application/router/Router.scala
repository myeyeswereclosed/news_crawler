package news_crawler.application.router

import cats.effect._
import cats.implicits._
import io.circe.syntax._
import news_crawler.request_processing.JsonRequestProcessor
import org.http4s.circe._
import org.http4s.dsl.Http4sDsl
import org.http4s.{HttpRoutes, StaticFile}

class Router[F[_]: Async](requestProcessor: JsonRequestProcessor[F]) extends Http4sDsl[F] {

  def routes(): HttpRoutes[F] =
    HttpRoutes.of[F] {
      case GET -> Root =>
        StaticFile
          .fromResource("/assets/graphiql.html")
          .getOrElseF(NotFound())

      case req @ POST -> Root / "graphql" =>
        req.asJson.attempt.flatMap {
          case Right(json) =>
            requestProcessor
              .process(json)
              .attempt
              .flatMap(
                _.fold(
                  error => BadRequest(error.getMessage.asJson),
                  response => Ok(response)
                )
              )
          case Left(error) => BadRequest(error.getMessage.asJson)
        }
    }

}

object Router {

  def apply[F[_]: Async](requestProcessor: JsonRequestProcessor[F]) = new Router(requestProcessor)

}
