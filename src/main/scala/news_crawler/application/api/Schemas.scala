package news_crawler.application.api

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import cats.implicits._
import news_crawler.domain.NewsLink
import news_crawler.repository.NewsRepository
import sangria.schema._

object Schemas {

  object News {

    val news: ObjectType[Unit, NewsLink] =
      ObjectType(
        "News",
        fields[Unit, NewsLink](
          Field(
            name = "title",
            fieldType = StringType,
            description = "News title".some,
            resolve = _.value.title
          ),
          Field(
            name = "link",
            fieldType = StringType,
            description = "News link".some,
            resolve = _.value.link
          )
        )
      )

    val schema: Schema[Unit, NewsLink] = Schema(news)
  }

  object NewsQuery {

    private val news: ObjectType[NewsRepository[IO], Unit] =
      ObjectType(
        "Query",
        fields[NewsRepository[IO], Unit](
          Field(
            name = "news",
            fieldType = ListType(News.news),
            description = "News query".some,
            resolve = context => context.ctx.list().unsafeToFuture()
          )
        )
      )

    val schema: Schema[NewsRepository[IO], Unit] = Schema(news)
  }

  case class ContextSchema[T](schema: Schema[T, Unit], context: T)

}
