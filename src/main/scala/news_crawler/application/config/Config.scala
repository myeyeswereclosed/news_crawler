package news_crawler.application.config

import cats.effect.Sync
import pureconfig.ConfigSource
import pureconfig.generic.auto._

import scala.concurrent.duration.FiniteDuration

object Config {

  case class AppConfig(
    server: ServerConfig,
    migration: MigrationConfig,
    newsCrawling: NewsCrawlingConfig
  )

  case class NewsCrawlingConfig(interval: FiniteDuration, newsSource: NewsSourceConfig)

  case class ServerConfig(host: String, port: Int)

  case class MigrationConfig(url: String, user: String, password: String)

  case class NewsSourceConfig(url: String, content: NewsContentConfig)

  case class NewsContentConfig(newsClass: String, titleHeader: String, link: String)

  def load[F[_]: Sync]: F[AppConfig] = Sync[F].delay(ConfigSource.default.loadOrThrow[AppConfig])

}
