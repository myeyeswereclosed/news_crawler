package news_crawler.application.dependencies

import cats.effect.{Async, IO, LiftIO}
import io.getquill.{H2JdbcContext, SnakeCase}
import net.ruippeixotog.scalascraper.browser.{Browser, JsoupBrowser}
import news_crawler.application.config.Config.AppConfig
import news_crawler.application.api.Schemas.{ContextSchema, NewsQuery}
import news_crawler.application.migration.{FlywayMigrationTool, MigrationTool}
import news_crawler.application.router.Router
import news_crawler.crawling_system.client.{NewsSourceClient, NewsSourceSttpClient}
import news_crawler.crawling_system.parser.{NewsContentHtmlParser, NewsContentParser}
import news_crawler.crawling_system.{NewsCrawler, OneTimeCrawler, ScheduledCrawler}
import news_crawler.domain.NewsLink.News
import news_crawler.repository.{NewsRepository, QuillNewsRepository}
import news_crawler.request_processing.GraphqlRequestProcessor
import org.typelevel.log4cats.Logger

class Dependencies[F[_]: Async: LiftIO: Logger](config: AppConfig, dbContext: H2JdbcContext[SnakeCase.type]) {

  private lazy val browser: Browser = JsoupBrowser()

  private lazy val parser: NewsContentParser = NewsContentHtmlParser(browser, config.newsCrawling.newsSource.content)

//  private lazy val client: NewsSourceClient[F] = NewsSourceScrapingClient[F](browser, config.newsCrawling.newsSource)
  private lazy val client: NewsSourceClient[F] = NewsSourceSttpClient[F](config.newsCrawling.newsSource)

  private lazy val repository = QuillNewsRepository[IO](dbContext)

  private lazy val schema    = ContextSchema(schema = NewsQuery.schema, context = repository)
  private lazy val processor = GraphqlRequestProcessor[F, NewsRepository[IO]](schema)

  lazy val migrationTool: MigrationTool[F] = FlywayMigrationTool[F](config.migration)

  lazy val simpleCrawler: NewsCrawler[F, News]    = OneTimeCrawler[F](client, parser, repository)
  lazy val scheduledCrawler: NewsCrawler[F, Unit] = ScheduledCrawler(config.newsCrawling, simpleCrawler)

  lazy val router: Router[F] = Router(processor)

}

object Dependencies {

  def apply[F[_]: Async: LiftIO: Logger](
    config: AppConfig,
    dbContext: H2JdbcContext[SnakeCase.type]
  ) = new Dependencies[F](config, dbContext)

}
