package news_crawler.crawling_system.parser

import news_crawler.domain.NewsLink

trait NewsContentParser {

  def parse(content: String): Seq[NewsLink]

}
