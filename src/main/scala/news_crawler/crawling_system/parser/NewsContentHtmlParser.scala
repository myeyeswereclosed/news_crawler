package news_crawler.crawling_system.parser

import net.ruippeixotog.scalascraper.browser.Browser
import net.ruippeixotog.scalascraper.dsl.DSL._
import net.ruippeixotog.scalascraper.model.Element
import net.ruippeixotog.scalascraper.scraper.ContentExtractors._
import news_crawler.application.config.Config.NewsContentConfig
import news_crawler.domain.NewsLink

class NewsContentHtmlParser(browser: Browser, config: NewsContentConfig) extends NewsContentParser {

  def parse(content: String): Seq[NewsLink] =
    (browser.parseString(content) >> elementList(config.newsClass))
      .flatMap(_.children.flatMap(parsedChild))

  private def parsedChild(child: Element): Option[NewsLink] =
    for {
      link  <- child >?> attr(config.link)
      title <- child >?> element(config.titleHeader).map(_.text)
    } yield NewsLink(title, link)

}

object NewsContentHtmlParser {

  def apply(browser: Browser, config: NewsContentConfig): NewsContentHtmlParser =
    new NewsContentHtmlParser(browser, config)

}
