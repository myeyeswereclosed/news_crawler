package news_crawler.crawling_system

import cats.effect.{IO, LiftIO}
import cats.effect.kernel.Sync
import cats.implicits._
import news_crawler.crawling_system.client.NewsSourceClient
import news_crawler.crawling_system.parser.NewsContentParser
import news_crawler.domain.NewsLink
import news_crawler.domain.NewsLink.News
import news_crawler.repository.NewsRepository
import org.typelevel.log4cats.Logger

class OneTimeCrawler[F[_]: Sync: LiftIO: Logger](
  client: NewsSourceClient[F],
  parser: NewsContentParser,
  repository: NewsRepository[IO]
) extends NewsCrawler[F, News] {

  def run(): F[News] =
    for {
      _           <- Logger[F].info("Starting news source crawling...")
      newsContent <- client.get()
      _           <- Logger[F].info("News content received")
      news = parser.parse(newsContent)
      freshNews <- news.headOption.map(_ => handleNewsReceived(news)).getOrElse(handleNoNewsReceived())
    } yield freshNews

  private def handleNewsReceived(news: News): F[News] =
    for {
      _ <- Logger[F].info(s"${news.size} news successfully parsed (unique ones ${news.distinctBy(_.link).size})")
      alreadyStored <- LiftIO[F].liftIO(repository.find(news))
      _ <-
        alreadyStored
          .headOption
          .map(_ => Logger[F].info(s"${alreadyStored.size} news are already stored, skipping them..."))
          .getOrElse(Sync[F].unit)
      freshNews = news.filterNot(n => alreadyStored.map(_.link).contains(n.link))
      _ <- Logger[F].info(s"${freshNews.size} fresh news received")
      _ <- freshNews.headOption.map(_ => LiftIO[F].liftIO(repository.add(freshNews))).getOrElse(Sync[F].pure(Seq()))
      _ <- Logger[F].info(s"${freshNews.size} fresh news stored")
    } yield freshNews

  private def handleNoNewsReceived(): F[News] = Logger[F].warn("No news received...") *> Sync[F].pure(Seq[NewsLink]())

}

object OneTimeCrawler {

  def apply[F[_]: Sync: LiftIO: Logger](
    client: NewsSourceClient[F],
    parser: NewsContentParser,
    repository: NewsRepository[IO]
  ) = new OneTimeCrawler[F](client, parser, repository)

}
