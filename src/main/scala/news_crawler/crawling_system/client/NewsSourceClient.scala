package news_crawler.crawling_system.client

trait NewsSourceClient[F[_]] {

  def get(): F[Content]

  type Content = String
}
