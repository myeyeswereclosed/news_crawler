package news_crawler.crawling_system.client

import cats.effect.Async
import cats.implicits._
import news_crawler.application.config.Config.NewsSourceConfig
import org.http4s.blaze.client.BlazeClientBuilder
import sttp.client3._
import sttp.client3.http4s.Http4sBackend

class NewsSourceSttpClient[F[_]: Async](config: NewsSourceConfig) extends NewsSourceClient[F] {

  def get(): F[String] =
    Http4sBackend
      .usingBlazeClientBuilder(BlazeClientBuilder[F])
      .use(backend =>
        basicRequest
          .get(uri"${config.url}")
          .send(backend)
          .flatMap(
            _.body.fold(
              error => Async[F].raiseError(new Throwable(error)),
              content => Async[F].pure(content)
            )
          )
      )

}

object NewsSourceSttpClient {

  def apply[F[_]: Async](config: NewsSourceConfig) = new NewsSourceSttpClient[F](config)

}
