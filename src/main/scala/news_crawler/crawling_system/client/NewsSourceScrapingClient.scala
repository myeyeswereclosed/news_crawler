package news_crawler.crawling_system.client

import cats.effect.kernel.Sync
import net.ruippeixotog.scalascraper.browser.Browser
import news_crawler.application.config.Config.NewsSourceConfig

class NewsSourceScrapingClient[F[_]: Sync](
  browser: Browser,
  config: NewsSourceConfig
) extends NewsSourceClient[F] {

  def get(): F[String] = Sync[F].blocking(browser.get(config.url).toHtml)

}

object NewsSourceScrapingClient {

  def apply[F[_]: Sync](browser: Browser, config: NewsSourceConfig) = new NewsSourceScrapingClient[F](browser, config)

}
