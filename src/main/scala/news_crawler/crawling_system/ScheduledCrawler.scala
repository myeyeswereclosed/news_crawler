package news_crawler.crawling_system

import cats.effect.Temporal
import cats.implicits._
import news_crawler.application.config.Config.NewsCrawlingConfig
import news_crawler.domain.NewsLink.News
import org.typelevel.log4cats.Logger

class ScheduledCrawler[F[_]: Temporal: Logger](
  config: NewsCrawlingConfig,
  crawler: NewsCrawler[F, News]
) extends NewsCrawler[F, Unit] {

  def run(): F[Unit] = crawlingStream().compile.drain

  private def crawlingStream() =
    fs2.Stream.eval(runCrawling()) ++
      fs2
        .Stream
        .awakeDelay[F](config.interval)
        .evalMap(_ => runCrawling())

  private def runCrawling() =
    crawler
      .run()
      .attempt
      .map(
        _.fold(
          error => Logger[F].error(s"Error ${error.getMessage} occurred"),
          _ => Logger[F].info(s"News were successfully crawled")
        )
      )

}

object ScheduledCrawler {

  def apply[F[_]: Temporal: Logger](config: NewsCrawlingConfig, crawler: NewsCrawler[F, News]) =
    new ScheduledCrawler(config, crawler)

}
