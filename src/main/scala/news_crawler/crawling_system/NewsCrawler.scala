package news_crawler.crawling_system

trait NewsCrawler[F[_], T] {

  def run(): F[T]

}
