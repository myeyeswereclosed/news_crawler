package news_crawler

import cats.effect._

object CatsEffectMain extends IOApp.Simple {
  def run: IO[Unit] = App.run[IO]
}
