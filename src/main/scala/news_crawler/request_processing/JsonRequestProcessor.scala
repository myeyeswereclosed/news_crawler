package news_crawler.request_processing

import io.circe.Json

trait JsonRequestProcessor[F[_]] {

  def process(request: Json): F[Json]

}
