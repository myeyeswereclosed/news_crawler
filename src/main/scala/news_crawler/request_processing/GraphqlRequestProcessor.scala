package news_crawler.request_processing

import cats.effect.{IO, LiftIO}
import io.circe.Json
import io.circe.optics.JsonPath._
import news_crawler.application.api.Schemas.ContextSchema
import sangria.ast.Document
import sangria.execution.Executor
import sangria.marshalling.circe._
import sangria.parser.QueryParser

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future

class GraphqlRequestProcessor[F[_]: LiftIO, T](contextSchema: ContextSchema[T]) extends JsonRequestProcessor[F] {

  def process(request: Json): F[Json] = {
    val queryString: Option[String] = root.query.string.getOption(request)

//    val operationName = root.operationName.string.getOption(request)
//    val variables     = root.variables.obj.getOption(request)

    val result: Future[Json] =
      queryString
        .map(query => parse(query).fold(error => Future.failed(error), execute))
        .getOrElse(Future.failed(new Throwable("No query to execute")))

    LiftIO[F].liftIO(IO.fromFuture(IO(result)))
  }

  private def execute(queryDocument: Document): Future[Json] =
    Executor
      .execute(
        schema = contextSchema.schema,
        queryAst = queryDocument,
        userContext = contextSchema.context
      )

  private def parse(requestQuery: String): Either[Throwable, Document] = QueryParser.parse(requestQuery).toEither

}

object GraphqlRequestProcessor {

  def apply[F[_]: LiftIO, T](contextSchema: ContextSchema[T]) = new GraphqlRequestProcessor[F, T](contextSchema)

}
