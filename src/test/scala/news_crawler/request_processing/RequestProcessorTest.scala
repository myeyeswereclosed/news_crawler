package news_crawler.request_processing

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import io.circe.Json
import io.circe.generic.auto._
import io.circe.parser._
import io.circe.syntax._
import news_crawler.application.api.Schemas.{ContextSchema, NewsQuery}
import news_crawler.domain.NewsLink
import news_crawler.repository.NewsRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers

class RequestProcessorTest extends AnyFreeSpecLike with MockFactory with Matchers {

  private val validRawRequest: String =
    """{
      |  "query" : "{\n  news {\n    title\n    link\n  }\n}",
      |  "variables" : null,
      |  "operationName" : null
      |}""".stripMargin

  private val validRequest = decode[Json](validRawRequest)

  private val repository = mock[NewsRepository[IO]]

  private val processor = GraphqlRequestProcessor(ContextSchema(NewsQuery.schema, repository))

  "Request processor should" - {

    "successfully process valid json request" in {

      validRequest.isRight shouldBe true

      val news = Seq(NewsLink("some_link", "some_title"))

      validRequest.map {
        request =>
          (repository.list _).expects().returns(IO.pure(news))

          val result = processor.process(request).unsafeRunSync()

          val resultDecoded = decode[NewsData](result.asJson.noSpaces)

          resultDecoded.map(_.data) shouldBe Right(News(news))
      }

    }

  }

  private case class NewsData(data: News)
  private case class News(news: Seq[NewsLink])

}
