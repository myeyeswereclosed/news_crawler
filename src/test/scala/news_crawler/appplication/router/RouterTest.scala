package news_crawler.appplication.router

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import io.circe._
import io.circe.generic.auto._
import io.circe.parser.decode
import io.circe.syntax._
import news_crawler.application.router.Router
import news_crawler.domain.NewsLink
import news_crawler.request_processing.JsonRequestProcessor
import org.http4s.{Method, Request, Response, Status, Uri}
import org.scalamock.scalatest.MockFactory
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers

class RouterTest extends AnyFreeSpecLike with Matchers with MockFactory {

  private val processor = mock[JsonRequestProcessor[IO]]
  private val router = Router[IO](processor)

  private val request: String =
    """{
      |  "query" : "{\n  news {\n    title\n    link\n  }\n}",
      |  "variables" : null,
      |  "operationName" : null
      |}""".stripMargin

  private val requestDecoded = decode[Json](request)

  private val responseJson = List(NewsLink("some_link", "some_title")).asJson

  "Router should" - {

    "route correct request to processor and respond with processing result" in {
      requestDecoded.isRight shouldBe true

      requestDecoded.map {
        requestJson =>
          (processor.process _).expects(requestJson).returns(IO.pure(responseJson))

          val response: Response[IO] =
            router
              .routes()
              .orNotFound
              .run(createRequest(request))
              .unsafeRunSync()

          response.status shouldBe Status.Ok
          response.body.compile.toList.unsafeRunSync() shouldBe responseJson.noSpaces.getBytes().toList
      }
    }

    "respond with bad request for incorrect request" in {
      val badRequestResponse =
        router
          .routes()
          .orNotFound
          .run(createRequest(""))
          .unsafeRunSync()

      badRequestResponse.status shouldBe Status.BadRequest
    }

    "respond with bad request if error occurred" in {
      requestDecoded.isRight shouldBe true

      requestDecoded.map {
        requestJson =>
          (processor.process _).expects(requestJson).returns(IO.raiseError(new Throwable("Some error")))

          val response: Response[IO] =
            router
              .routes()
              .orNotFound
              .run(createRequest(request))
              .unsafeRunSync()

          response.status shouldBe Status.BadRequest
      }
    }

  }

  private def createRequest(body: String) =
    Request[IO](
      method = Method.POST,
      uri = Uri.unsafeFromString(s"/graphql"),
      body = fs2.Stream.fromIterator[IO](body.getBytes().iterator, 4096)
    )
}
