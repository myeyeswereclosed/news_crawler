package news_crawler.appplication.migration

import better.files.File
import cats.effect.IO
import cats.effect.unsafe.implicits.global
import news_crawler.application.config.Config.MigrationConfig
import news_crawler.application.migration.FlywayMigrationTool
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers

class MigrationToolTest extends AnyFreeSpecLike with Matchers {

  private val dbName = "test_db"
  private val migrationConfig = MigrationConfig(url = s"jdbc:h2:./$dbName", user = "sa", password = "")

  "Migration tool should" - {

    "run migrations" in {
      File.currentWorkingDirectory.list(_.nameWithoutExtension == dbName) shouldBe empty

      val migrationTool = FlywayMigrationTool[IO](migrationConfig)

      migrationTool.migrate().unsafeRunSync()

      val dbFile = File.currentWorkingDirectory.list.find(_.nameWithoutExtension == dbName)

      dbFile.nonEmpty shouldBe true

      dbFile.foreach(_.delete())
    }

  }

}
