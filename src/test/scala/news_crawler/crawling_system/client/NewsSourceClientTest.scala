package news_crawler.crawling_system.client

import cats.effect.IO
import cats.effect.unsafe.implicits.global
import news_crawler.application.config.Config.{NewsContentConfig, NewsSourceConfig}
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers

import scala.util.Try

class NewsSourceClientTest extends AnyFreeSpecLike with Matchers {

  private val correctUrl = "https://www.nytimes.com"

  private val config: NewsSourceConfig =
    NewsSourceConfig(
      url = correctUrl,
      content = NewsContentConfig("some_class", "some_header", "some_link")
    )

  "News source client" - {

    "should receive content from correct url" in {

      val client = NewsSourceSttpClient[IO](config)

      client.get().unsafeRunSync() should not be empty

    }

    "should end up with error for incorrect url" in {

      val client = NewsSourceSttpClient[IO](config.copy(url = "http://123"))

      Try { client.get().unsafeRunSync() }.fold(
        _ => (),
        _ => fail()
      )
    }

  }

}
