package news_crawler.crawling_system

import cats.effect.unsafe.implicits.global
import cats.effect.{IO, Sync}
import news_crawler.crawling_system.client.NewsSourceClient
import news_crawler.crawling_system.parser.NewsContentParser
import news_crawler.domain.NewsLink
import news_crawler.repository.NewsRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

class OneTimeCrawlerTest extends AnyFreeSpecLike with Matchers with MockFactory {

  implicit def logger[F[_]: Sync]: SelfAwareStructuredLogger[F] = Slf4jLogger.getLogger[F]

  private val client = mock[NewsSourceClient[IO]]
  private val parser = mock[NewsContentParser]
  private val repository = mock[NewsRepository[IO]]

  private val content = "Stub content"
  private val newsParsed =
    Seq(
      NewsLink(link = "stub_link_1", title = "Good news"),
      NewsLink(link = "stub_link_2", title = "Bad news"),
    )

  private val crawler = OneTimeCrawler[IO](client, parser, repository)

  "Simple crawler should" - {

    "successfully store all news if repository is empty" in {
      (client.get _).expects().returns(IO.pure(content))
      (parser.parse _).expects(content).returns(newsParsed)
      (repository.find _).expects(newsParsed).returns(IO.pure(Seq()))
      (repository.add _).expects(newsParsed).returns(IO.pure(newsParsed))

      crawler.run().unsafeRunSync() shouldBe newsParsed
    }

    "not store already stored news" in {
      (client.get _).expects().returns(IO.pure(content))
      (parser.parse _).expects(content).returns(newsParsed)
      (repository.find _).expects(newsParsed).returns(IO.pure(newsParsed))

      crawler.run().unsafeRunSync() shouldBe empty
    }

    "successfully store only fresh news" in {
      val expectedFreshNews = Seq(newsParsed.tail.head)

      (client.get _).expects().returns(IO.pure(content))
      (parser.parse _).expects(content).returns(newsParsed)
      (repository.find _).expects(newsParsed).returns(IO.pure(Seq(newsParsed.head)))
      (repository.add _).expects(expectedFreshNews).returns(IO.pure(expectedFreshNews))

      crawler.run().unsafeRunSync() shouldBe expectedFreshNews
    }

    "handle if there are no news" in {
      (client.get _).expects().returns(IO.pure(content))
      (parser.parse _).expects(content).returns(Seq())

      crawler.run().unsafeRunSync() shouldBe empty

    }
  }

}
