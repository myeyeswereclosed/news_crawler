package news_crawler.crawling_system.parser

import net.ruippeixotog.scalascraper.browser.JsoupBrowser
import news_crawler.application.config.Config.NewsContentConfig
import news_crawler.domain.NewsLink
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers

class NewsPageParserTest extends AnyFreeSpecLike with Matchers {

  private val link = "https://www.nytimes.com/2022/04/19/us/politics/som_news"
  private val title = "There are some news"

  private val config = NewsContentConfig(newsClass = ".story-wrapper", titleHeader = "h3", link = "href")

  private val simplePageContent =
    s"""<section class="story-wrapper">
      | <a class="css-1xl8npp" href="$link">
      |   <div>
      |     <div class="css-xdandi">
      |       <h3 class="indicate-hover css-1pvrrwb">$title</h3>
      |     </div>
      |   </div>
      | </a>
      |</section>""".stripMargin

  "News page parser should" - {

    "parse simple new page" in {
      val parser = NewsContentHtmlParser(JsoupBrowser(), config)

      parser.parse(simplePageContent) shouldBe Seq(NewsLink(title, link))
    }

  }

}
