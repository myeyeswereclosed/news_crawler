package news_crawler.crawling_system

import cats.effect.unsafe.implicits.global
import cats.effect.{IO, Sync}
import news_crawler.application.config.Config.{NewsContentConfig, NewsCrawlingConfig, NewsSourceConfig}
import news_crawler.domain.NewsLink
import news_crawler.domain.NewsLink.News
import org.scalamock.scalatest.MockFactory
import org.scalatest.freespec.AnyFreeSpecLike
import org.scalatest.matchers.should.Matchers
import org.typelevel.log4cats.SelfAwareStructuredLogger
import org.typelevel.log4cats.slf4j.Slf4jLogger

import scala.concurrent.duration._

class ScheduledCrawlerTest extends AnyFreeSpecLike with Matchers with MockFactory {

  implicit def logger[F[_]: Sync]: SelfAwareStructuredLogger[F] = Slf4jLogger.getLogger[F]

  private val config =
    NewsCrawlingConfig(
      interval = 2 seconds,
      NewsSourceConfig(
        url = "stub_url",
        content = NewsContentConfig("some_class", "some_header", "some_link")
      )
    )
  private val simpleCrawler = mock[NewsCrawler[IO, News]]

  private val scheduledCrawler = ScheduledCrawler[IO](config, simpleCrawler)

  "Scheduled crawler should" - {

    "run according to schedule" in {
      (simpleCrawler.run _).expects().twice().returns(IO.pure(Seq(NewsLink("stub_link", "stub_title"))))

      (for {
        cancelToken <- scheduledCrawler.run().start
        _ <- IO.sleep(4 seconds)
        _ <- cancelToken.cancel
      } yield ()).unsafeRunSync()
    }

    "run despite errors" in {
      (simpleCrawler.run _).expects().returns(IO.raiseError(new Throwable("Some error")))
      (simpleCrawler.run _).expects().returns(IO.pure(Seq(NewsLink("stub_link", "stub_title"))))

      (for {
        cancelToken <- scheduledCrawler.run().start
        _ <- IO.sleep(4 seconds)
        _ <- cancelToken.cancel
      } yield ()).unsafeRunSync()
    }

  }

}
